<?php

class Customer_Model extends CI_Model
{
    private $tcus = "users";
    private $tpo = "point";
    public function getCustomer($id = null, $ida = null)
    {
        if ($id == null) {
            $this->db->select('users.id , users.username,users.password,users.email,users.level ,users.idadmin, point.point');
            $this->db->from($this->tcus);
            $this->db->join($this->tpo, 'users.id = point.iduser');
            $this->db->where('users.idadmin ', $ida);
            $cus = $this->db->get();
            return $cus->result_array();
        } else {
            if (is_numeric($id)) {
                $this->db->select('users.id , users.username,users.password,users.email,users.level ,users.idadmin, point.point');
                $this->db->from($this->tcus);
                $this->db->join($this->tpo, 'users.id = point.iduser');
                $this->db->where('users.id ', $id);
                $cus = $this->db->get();
                $cuss = $cus->result();
                return $cuss;
            } else {
                $this->db->select('users.id , users.username,users.password,users.email,users.level ,users.idadmin, point.point');
                $this->db->from($this->tcus);
                $this->db->join($this->tpo, 'users.id = point.iduser');
                $this->db->where('users.username ', $id);
                $cus = $this->db->get();
                return $cus->result();
            }
        }
    }
    public function addCustomer($data)
    {
        $this->db->insert($this->tcus, $data);
        return $this->db->insert_id();
    }
    public function deleteCustomer($id)
    {
        $this->db->where("id", $id);
        $this->db->delete($this->tcus);
    }
    public function deletePoint($id)
    {
        $this->db->where("iduser", $id);
        $this->db->delete($this->tpo);
    }
    public function editCustomer($data, $id)
    {
        $this->db->where("id", $id);
        $this->db->update($this->tcus, $data);
    }
    public function getHistory($idu, $type)
    {
        if ($type == "hadiah") {
            $this->db->select('hadiah.reward, hadiah.point, hadiah.gambar');
            $this->db->from("tukar");
            $this->db->join("hadiah", 'hadiah.id = tukar.idhadiah');
            $this->db->where('tukar.iduser ', $idu);
            $cus = $this->db->get();
            return $cus->result_array();
        } elseif ($type == "transaksi") {
            $this->db->select('transaksi.nama, transaksi.jumlah,transaksi.total,transaksi.tgltrans');
            $this->db->from($this->tcus);
            $this->db->join("transaksi", 'users.id = transaksi.iduser');
            $this->db->where('users.id ', $idu);
            $cus = $this->db->get();
            return $cus->result_array();
        }
    }
}