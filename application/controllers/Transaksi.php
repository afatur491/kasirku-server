<?php

use chriskacerguis\RestServer\RestController;

class Transaksi extends RestController
{
    public function __construct()
    {
        parent::__construct();
        $this->getHeader()->run();
        $level = $this->currentUser()->level;
        if ($level != "admin") {
            $res = $this->res->send(0, "You Are Not an Admin");
            return $this->response($res, 200);
        }
    }
    public function index_get($idu = null, $id = null)
    {
        $transaksi = $this->Transaksi_Model->getTransaksi($idu, $id, $this->currentUser()->id);
        if ($transaksi) {
            $res = $this->res->send(1, "Transaksi Was Found", $transaksi);
            return $this->response($res, 200);
        } else {
            $res = $this->res->send(0, "Transaksi Not Found");
            return $this->response($res, 200);
        }
    }
    public function add_post()
    {
        $data['iduser'] = $this->input->post('iduser');
        $data['nama'] = $this->input->post('nama');
        $data['jumlah'] = $this->input->post('jumlah');
        $data['total'] = $this->input->post('total');
        $data['tgltrans'] = date("d F Y");

        $this->form_validation->set_rules('iduser', 'Username', 'required');
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('jumlah', 'Jumlah', 'required|numeric');
        $this->form_validation->set_rules('total', 'Total', 'required|numeric');
        if ($this->form_validation->run() == FALSE) {
            $res = $this->res->send(0, array_values($this->form_validation->error_array())[0]);
            return $this->response($res, 200);
        }
        $dapat = $this->Customer_Model->getCustomer($data['iduser']);

        if (!$dapat) {
            $res = $this->res->send(0, 'ID Customer Not Found');
            return $this->response($res, 200);
        }
        $this->Transaksi_Model->save($data);
        $this->Transaksi_Model->addPoint($data['iduser'], 'plus', 5);
        $res = $this->res->send(1, 'Transaksi Success');
        return $this->response($res, 200);
    }
    public function getprize_get($id = null, $point = null)
    {
        $dapat = $this->Transaksi_Model->getHadiah($id, $point, $this->currentUser()->id);

        if (!$dapat) {
            $res = $this->res->send(0, 'Failed Get Prize Data', $dapat);
            return $this->response($res, 200);
        }
        $res = $this->res->send(1, 'Get Prize Success', $dapat);
        return $this->response($res, 200);
    }
    public function addprize_post()
    {
        $data['point'] = $this->input->post('point');
        $data['reward'] = $this->input->post('reward');
        $data['idadmin'] = $this->currentUser()->id;

        $this->form_validation->set_rules('point', 'Point', 'required|numeric');
        $this->form_validation->set_rules('reward', 'Reward', 'required');
        $this->form_validation->set_rules('gambar', 'Image', 'required');

        if ($this->form_validation->run() == FALSE) {
            $res = $this->res->send(0, array_values($this->form_validation->error_array())[0]);
            return $this->response($res, 200);
        }
        $data['gambar'] = $this->upload();
        if ($data['gambar'] == false) {
            $res = $this->res->send(0, 'Your Uploaded is Not Image');
            return $this->response($res, 200);
        }

        $this->Transaksi_Model->addHadiah($data);
        $res = $this->res->send(1, 'New Prize Added');
        return $this->response($res, 200);
    }

    public function deleteprize_delete()
    {
        $id = $this->delete('id');

        if ($id == null) {
            $res = $this->res->send(0, 'Field Id Required');
            return $this->response($res, 200);
        }
        $gambarlama = $this->Transaksi_Model->getHadiah($id);
        unlink(FCPATH . $gambarlama[0]->gambar);
        $this->Transaksi_Model->deleteHadiah($id);
        $res = $this->res->send(1, 'Prize Deleted');
        return $this->response($res, 200);
    }
    public function upload()
    {
        $gambar  = $this->input->post('gambar');
        $newGambar = explode('data:image/', $gambar);
        $newGambar = explode(';base64,', $newGambar[1]);
        $gambartype = $newGambar[0];
        $gambarNama = $newGambar[1];
        $gambarNewNama = 'assets/img/' . substr(md5(microtime()), rand(0, 26), 5) . "." . $gambartype;
        if ($gambartype != ('jpeg' || 'jpg' || 'png')) {
            return false;
        } else {
            file_put_contents('./' . $gambarNewNama, base64_decode($gambarNama));
            return $gambarNewNama;
        }
        // return $gambarNewNama;
    }
    public function tukar_post()
    {
        $iduser = $this->input->post('iduser');
        $idhadiah = $this->input->post('idhadiah');

        $this->form_validation->set_rules('iduser', 'iduser', 'required|numeric');
        $this->form_validation->set_rules('idhadiah', 'idhadiah', 'required|numeric');

        if ($this->form_validation->run() == FALSE) {
            $res = $this->res->send(0, array_values($this->form_validation->error_array())[0]);
            return $this->response($res, 200);
        }

        $getcus  = $this->Customer_Model->getCustomer($iduser);
        $gethadiah =  $this->Transaksi_Model->getHadiah($idhadiah, "", 4);
        if ($getcus[0]->point < $gethadiah[0]->point) {
            $res = $this->res->send(0, 'Your point is not enough to get this prize');
            return $this->response($res, 200);
        }
        $this->Transaksi_Model->tukar($iduser, $idhadiah);
        $this->Transaksi_Model->addPoint($iduser, 'min', $gethadiah[0]->point);
        $res = $this->res->send(1, 'Point Exchanged');
        return $this->response($res, 200);
    }
}