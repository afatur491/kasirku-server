<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Users_Model extends CI_Model
{
    private $table = "admin";
    private $tpo = "point";
    public function getbyusername($email, $re)
    {
        $dataemail = $this->db->get_where($this->table, array('username' => $email));
        if ($re == "email") {
            return  $dataemail->result();
        } elseif ($re == "row") {
            return $dataemail->num_rows();
        }
    }
    public function getbyid($id)
    {
        $dataadmin = $this->db->get_where($this->table, array('id' => $id));
        return  $dataadmin->result();
    }
    public function save($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
    public function isitoken($id, $token)
    {
        $this->db->where('id', $id);
        $this->db->update($this->table, array('token' => $token));
    }
    public function addPoint($id)
    {
        $data = array(
            "iduser"    =>  $id,
            "point"     =>  0
        );
        $this->db->insert($this->tpo, $data);
    }
}