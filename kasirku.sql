-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 03 Agu 2020 pada 05.27
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kasirku`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `level` enum('admin') NOT NULL,
  `token` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `email`, `level`, `token`) VALUES
(1, 'admin', '$2y$10$IskBnG6SoAbO3SDgKIbBYuEuqQ3PbAPQhO3A8fZdeIr.3vmc/HhTi', 'admin', 'admin', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9leGFtcGxlLm9yZyIsImF1ZCI6Imh0dHA6XC9cL2V4YW1wbGUuY29tIiwiaWF0IjoxNTk2MzQxNTY1LCJuYmYiOjE1OTYzNDE1NzUsImRhdGEiOnsiaWQiOjEsImxldmVsIjoiYWRtaW4ifX0.0HKcud9X2Nj2P1W9T9Xdptxl27vdYNDiysRkCktWZyY'),
(2, 'cus', '$2y$10$hvi3HJMj5pHu9bDPlK.poOTGam2jgVRUm35dGob6A8s6.E6JYGK3a', 'cus', 'admin', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9leGFtcGxlLm9yZyIsImF1ZCI6Imh0dHA6XC9cL2V4YW1wbGUuY29tIiwiaWF0IjoxNTk2MzUyOTc0LCJuYmYiOjE1OTYzNTI5ODQsImRhdGEiOnsiaWQiOjIsImxldmVsIjoiYWRtaW4ifX0.LEnxu7ig-gZNz8MMJfh2KSx9EjrzT6a2UGFGYhLfCXY'),
(3, 'saipul', '$2y$10$xvNogucNrrJxv7.Xp7dXQuqjdDKGBQxDE0v7ZU1nUJsRrQQMTSEOK', 'saipul', 'admin', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9leGFtcGxlLm9yZyIsImF1ZCI6Imh0dHA6XC9cL2V4YW1wbGUuY29tIiwiaWF0IjoxNTk2Mzg3NTMwLCJuYmYiOjE1OTYzODc1NDAsImRhdGEiOnsiaWQiOjMsImxldmVsIjoiYWRtaW4ifX0.HU1_DeROfHAd3CnXUx3RBqJsnh8Em7kNhSwVrJzeyjE');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hadiah`
--

CREATE TABLE `hadiah` (
  `id` int(11) NOT NULL,
  `point` int(11) NOT NULL,
  `reward` varchar(128) NOT NULL,
  `gambar` text NOT NULL,
  `idadmin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `hadiah`
--

INSERT INTO `hadiah` (`id`, `point`, `reward`, `gambar`, `idadmin`) VALUES
(1, 56, 'rumah', 'assets/img/627db.jpeg', 0),
(2, 4, 'Sapi', 'assets/img/62a45.jpeg', 1),
(3, 56, 'rumah', 'assets/img/265b5.jpeg', 2),
(4, 5, 'sepatu', 'assets/img/c06d5.jpeg', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `point`
--

CREATE TABLE `point` (
  `id` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `point` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `point`
--

INSERT INTO `point` (`id`, `iduser`, `point`) VALUES
(1, 1, 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `id` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `tgltrans` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`id`, `iduser`, `nama`, `jumlah`, `total`, `tgltrans`) VALUES
(7, 25, 'topi', 5, 45, '02 August 2020'),
(8, 25, 'sepatu', 6, 45, '02 August 2020'),
(9, 26, 'said', 2, 888, '02 August 2020'),
(10, 26, 'sepatu', 1, 3, '02 August 2020'),
(11, 26, 'sepeda', 2, 23, '02 August 2020'),
(12, 26, 'sapi', 4, 2, '02 August 2020'),
(13, 1, 'sapi', 2, 34, '02 August 2020');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tukar`
--

CREATE TABLE `tukar` (
  `id` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `idhadiah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tukar`
--

INSERT INTO `tukar` (`id`, `iduser`, `idhadiah`) VALUES
(1, 25, 4),
(2, 25, 2),
(3, 25, 2),
(4, 26, 2),
(5, 25, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `level` enum('admin','user') NOT NULL,
  `idadmin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `level`, `idadmin`) VALUES
(1, 'rendang', '$2y$10$w5XVDZzAzEpHIIuvdc4Ll.pJvAmSqJX54/3q2M1MNkSwI/y92AlSu', 'rendang', 'user', 1);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `hadiah`
--
ALTER TABLE `hadiah`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `point`
--
ALTER TABLE `point`
  ADD PRIMARY KEY (`id`),
  ADD KEY `iduser` (`iduser`);

--
-- Indeks untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `iduser` (`iduser`);

--
-- Indeks untuk tabel `tukar`
--
ALTER TABLE `tukar`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `hadiah`
--
ALTER TABLE `hadiah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `point`
--
ALTER TABLE `point`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `tukar`
--
ALTER TABLE `tukar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
