<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Customer extends RestController
{
    public function __construct()
    {
        parent::__construct();
        $this->getHeader()->run();
        $level = $this->currentUser()->level;
        if ($level != "admin") {
            $res = $this->res->send(0, "You Are Not an Admin");
            return $this->response($res, 200);
        }
    }
    public function index_get($id = null)
    {
        $customer = $this->Customer_Model->getCustomer($id, $this->currentUser()->id);
        if ($customer) {
            $res = $this->res->send(1, "Customer Was Found", $customer);
            return $this->response($res, 200);
        } else {
            $res = $this->res->send(0, "Customer Not Found",);
            return $this->response($res, 200);
        }
    }
    public function add_post()
    {
        $data["username"] = $this->input->post('username');
        $data["password"] = password_hash($this->input->post('password'), true);
        $data["email"] = $this->input->post('email');
        $data["level"] = "user";
        $data["idadmin"] = $this->currentUser()->id;

        $this->form_validation->set_rules('username', "Username", "required");
        $this->form_validation->set_rules('password', "Password", "required");
        $this->form_validation->set_rules('email', "Email", "required|valid_email");

        if ($this->form_validation->run() == FALSE) {
            $res = $this->res->send(0, array_values($this->form_validation->error_array())[0]);
            return $this->response($res, 200);
        }
        if ($this->Users_Model->getbyusername($data['username'], "row") > 0) {
            $res = $this->res->send(0, "Username Already Exist");
            return $this->response($res, 200);
        }
        if ($this->Customer_Model->getCustomer($data['username'])) {
            $res = $this->res->send(0, "Username Already Exist");
            return $this->response($res, 200);
        }
        $id  = $this->Customer_Model->addCustomer($data);
        $this->Users_Model->addPoint($id);
        $res = $this->res->send(1, "Account Created");
        return $this->response($res, 200);
    }
    public function delete_delete()
    {
        $id = $this->delete('id');

        if ($id == null) {
            $res = $this->res->send(0, "Id is Nothing");
            return $this->response($res, 200);
        }
        $dapat = $this->Customer_Model->getCustomer($id, $this->currentUser()->id);
        if (!$dapat) {
            $res = $this->res->send(0, "Customer not exist",);
            return $this->response($res, 200);
            $level = $this->currentUser()->level;
        }
        $this->Customer_Model->deletePoint($id);
        $this->Customer_Model->deleteCustomer($id);
        $res = $this->res->send(1, "Customer deleted");
        return $this->response($res, 200);
    }
    public function edit_put()
    {
        $this->form_validation->set_data($this->put());
        $this->form_validation->set_rules('username', "Username", "required");
        $this->form_validation->set_rules('password', "Password", "required");
        $this->form_validation->set_rules('email', "Email", "required|valid_email");

        if ($this->form_validation->run() == FALSE) {
            $res = $this->res->send(0, array_values($this->form_validation->error_array())[0]);
            return $this->response($res, 200);
        }
        $id = $this->put('id');
        $data['username'] = $this->put('username');
        $data['password'] = password_hash($this->put('password'), true);
        $oldpassword = $this->put('oldpassword');
        $data['email'] = $this->put('email');
        $dapat = $this->Customer_Model->getCustomer($id, $this->currentUser()->id);
        if (!$dapat) {
            $res = $this->res->send(0, "Customer Not Exist");
            return $this->response($res, 200);
        }
        if ($dapat[0]->username != $data['username']) {
            if ($this->Users_Model->getbyusername($data['username'], "row") > 0) {
                $res = $this->res->send(0, "Username Already Exist");
                return $this->response($res, 200);
            } else {
                if (!password_verify($oldpassword, $dapat[0]->password)) {
                    $res = $this->res->send(0, "Wrong Old Password");
                    return $this->response($res, 200);
                } else {
                    $this->Customer_Model->editCustomer($data, $id);
                    $res = $this->res->send(1, "Customer Updated");
                    return $this->response($res, 200);
                }
            }
        }
    }
    public function history_get($idu, $type)
    {
        if ($idu == null || $type == null) {
            $res = $this->res->send(0, "History Request Failed");
            return $this->response($res, 200);
        }
        $datahis = $this->Customer_Model->getHistory($idu, $type);
        $res = $this->res->send(1, "History obtained", $datahis);
        return $this->response($res, 200);
    }
}