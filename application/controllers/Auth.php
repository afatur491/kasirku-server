<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Auth extends RestController
{
    public function __construct()
    {
        parent::__construct();
    }
    public function register_post()
    {
        $data["username"] = $this->input->post('username');
        $data["password"] = password_hash($this->input->post('password'), true);
        $data["email"] = $this->input->post('email');
        $data["level"] = "admin";

        $this->form_validation->set_rules('username', "Username", "required");
        $this->form_validation->set_rules('password', "Password", "required");
        $this->form_validation->set_rules('email', "Email", "required");

        if ($this->form_validation->run() == FALSE) {
            $res = $this->res->send(0, array_values($this->form_validation->error_array())[0]);
            return $this->response($res, 200);
        }
        if ($this->Users_Model->getbyusername($data['username'], "row") > 0) {
            $res = $this->res->send(0, "Username Already Exist");
            return $this->response($res, 200);
        }
        $id  = $this->Users_Model->save($data);
        $jwt_token = new JWT_token();
        $jwt_token->init([
            'id' => $id,
            'level' => $data["level"],
        ]);
        $token = $jwt_token->get_token();
        $this->Users_Model->isitoken($id, $token);
        $res = $this->res->send(1, "Admin Account Created");
        return $this->response($res, 200);
    }
    public function login_post()
    {
        $data["password"] = $this->input->post('password');
        $data["username"] = $this->input->post('username');

        $this->form_validation->set_rules('password', "password", "required");
        $this->form_validation->set_rules('username', "Username", "required");

        if ($this->form_validation->run() == FALSE) {
            $res = $this->res->send(0, array_values($this->form_validation->error_array())[0]);
            return $this->response($res, 200);
        }
        if ($this->Users_Model->getbyusername($data['username'], "row") < 1) {
            $customer = $this->Customer_Model->getCustomer($data['username']);
            if (!$customer) {
                $res = $this->res->send(0, "Username Not Register");
                return $this->response($res, 200);
            } else {
                if (!password_verify($data['password'], $customer[0]->password)) {
                    $res = $this->res->send(0, "Wrong Password");
                    return $this->response($res, 200);
                } else {
                    $isicustomer = array(
                        "idadmin" => $customer[0]->idadmin,
                        "level" => $customer[0]->level,
                        "id" => $customer[0]->id,
                    );
                    $res = $this->res->send(1, "Login Success", $isicustomer);
                    return $this->response($res, 200);
                }
            }
        } else {
            $getuser = $this->Users_Model->getbyusername($data['username'], "email")[0];
            if (!password_verify($data['password'], $getuser->password)) {
                $res = $this->res->send(0, "Wrong Password");
                return $this->response($res, 200);
            } else {
                $islogin = array(
                    "token" => $getuser->token,
                    "level" => $getuser->level,
                    "id" => $getuser->id,
                );
                $res = $this->res->send(1, "Login Success", $islogin);
                return $this->response($res, 200);
            }
        }
    }
    public function getdata_get($id)
    {
        $bio = $this->Users_Model->getbyid($id);
        if (!$bio) {
            $res = $this->res->send(0, "Get Data Admin Failed");
            return $this->response($res, 200);
        }
        $res = $this->res->send(1, "Get Data Admin Success", $bio);
        return $this->response($res, 200);
    }
}