<?php

use \Firebase\JWT\JWT;

class JWT_token
{

    private $CI;
    public $issuedat_claim, $notbefore_claim, $expire_claim, $key, $payload;

    public function __construct()
    {
        $this->CI = get_instance();
        $this->key = "example_key";
    }

    public function init($data)
    {
        $this->issuedat_claim = time(); // issued at
        $this->notbefore_claim = $this->issuedat_claim + 10; //not before in seconds
        $this->expire_claim = $this->issuedat_claim + 3600; // expire time in seconds
        $this->payload = array(
            "iss" => "http://example.org",
            "aud" => "http://example.com",
            "iat" => $this->issuedat_claim,
            "nbf" => $this->notbefore_claim,
            // "exp" => $this->expire_claim,
            'data' => $data
        );
    }

    public function get_token()
    {
        return
            JWT::encode($this->payload, $this->key);
    }

    public function get_exp()
    {
        return $this->expire_claim;
    }

    public function result()
    {
        return [
            'token' => $this->get_token(),
            'exp' => $this->get_exp()
        ];
    }

    public function decode($token, $key)
    {
        return JWT::decode($token, $key, ['HS256']);
    }
}