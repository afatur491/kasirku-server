<?php

require APPPATH . 'libraries/Res.php';

class ApiHeader
{
    public $isApi;
    private $res, $token;
    static $decoded;

    public function __construct()
    {
        $this->isApi = false;
        $this->res = new Res();
        $this->token = $_SERVER['HTTP_X_AUTH'] ?? null;
    }

    public function setApi()
    {
        $this->isApi = true;
    }

    public function run()
    {
        $token = $this->token;
        $jwt = new JWT_token();
        $secret_key = $jwt->key;


        if (!empty($token)) {
            try {
                self::$decoded = $jwt->decode($token, $secret_key);
                // Access is granted. Add code of the operation here
            } catch (\Exception $e) {
                $res = $this->res->send(0, $e->getMessage());
                return $this->response($res, 401);
            }
        } else {
            $res = $this->res->send(0, 'Access deny, please insert the api token');
            return $this->response($res, 401);
        }
    }

    public function response($res)
    {
        header('Content-Type: application/json');
        echo json_encode($res);
        die();
    }

    public function read()
    {
        return self::$decoded;
    }

    public function getId()
    {
        return $this->read()->data->id;
    }

    public function validate($token)
    {
        $jwt = new JWT_token();
        $secret_key = $jwt->key;

        if (!empty($token)) {
            try {
                return $jwt->decode($token, $secret_key);

                // Access is granted. Add code of the operation here
            } catch (\Exception $e) {
                return false;
            }
        } else {
            return false;
        }
    }
}