<?php
class Res
{
    public $CI;
    public function __construct()
    {
        $this->CI = get_instance();
    }
    public function send($status, $pesan, $data = [])
    {
        return array(
            "status"  => $status,
            "pesan" => $pesan,
            "data"  => $data
        );
    }
}