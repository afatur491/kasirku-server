<?php
class Transaksi_Model extends CI_Model
{
    private $ttrans = "transaksi";
    private $thad = "hadiah";
    public function getTransaksi($idu = null, $id = null, $ida)
    {
        if ($id == null) {
            $this->db->select('users.username, transaksi.nama,transaksi.jumlah ,transaksi.total,transaksi.tgltrans');
            $this->db->from('users');
            $this->db->join($this->ttrans, 'users.id = transaksi.iduser');
            $this->db->where('users.idadmin ', $ida);
            return $this->db->get()->result_array();
        } elseif ($id == null && $idu != null) {
            return $this->db->get_where($this->ttrans, array("iduser" => $idu))->result();
        } else {
            return $this->db->get_where($this->ttrans, array("id" => $id, "iduser" => $idu))->result();
        }
    }
    public function save($data)
    {
        $this->db->insert($this->ttrans, $data);
    }
    public function getHadiah($id = null, $point = null, $ida)
    {
        if ($id == null && $point == null) {
            $hadiah = $this->db->get_where($this->thad, array('idadmin' => $ida));
            return $hadiah->result_array();
        } elseif ($id != null && $point == null) {
            $hadiah = $this->db->get_where($this->thad, array('id' => $id));
            return $hadiah->result();
        } elseif (($id != null && $point != null)) {
            if (is_numeric($id)) {
                $hadiah = $this->db->get_where($this->thad, array('id' => $id));
                return $hadiah->result();
            } else {
                $hadiah = $this->db->query("SELECT * FROM $this->thad WHERE point <= $point and idadmin=$ida");
                return $hadiah->result_array();
            }
        }
    }
    public function addHadiah($data)
    {
        $this->db->insert($this->thad, $data);
    }
    public function deleteHadiah($id)
    {
        $this->db->delete($this->thad, array('id' => $id));
    }
    public function addPoint($id, $or, $num)
    {
        if ($or == 'plus') {
            $this->db->query("UPDATE point SET point = point + $num WHERE iduser = $id");
        } elseif ($or == 'min') {
            $this->db->query("UPDATE point SET point = point - $num WHERE iduser = $id");
        }
    }
    public function tukar($idu, $idh)
    {
        $data = [
            "iduser" => $idu,
            "idhadiah"  =>  $idh
        ];
        $this->db->insert("tukar", $data);
    }
}